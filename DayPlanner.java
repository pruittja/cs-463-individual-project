package CalendarApp;

import java.util.TreeMap;

// A Java class used to hold appointments
// and separate them based on their dates
// and times. 
public class DayPlanner {

	private TreeMap<String, Appointment> appointmentList;
	
	public DayPlanner () {
		
		appointmentList = new TreeMap<String, Appointment>();
		
	}
	
	public DayPlanner(TreeMap<String, Appointment> list) {
		
		appointmentList = list;
		
	}
	
	public Appointment add(String dateTime, Appointment appointment) {
		
		return appointmentList.put(dateTime, appointment);
		
	}
	
	public Appointment remove(String key) {
		
		return appointmentList.remove(key);
		
	}
	
	public boolean isEmpty() {
		
		return appointmentList.isEmpty();
		
	}
	
	
	public TreeMap<String, Appointment> getAppointmentList() {
		
		return appointmentList;
		
	}
	
	public void setAppointmentList(TreeMap<String, Appointment> appList) {
		
		appointmentList = appList;
		
	}
	
}
