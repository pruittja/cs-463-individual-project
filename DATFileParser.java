package CalendarApp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.swing.JFileChooser;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public class DATFileParser implements DataParser {
	
	public boolean saveData(TreeMap list) {

		FileDialog chooser = new FileDialog(new Shell(), SWT.SAVE);
		
		String fileName = chooser.open();
		
		if (fileName == null) {
			
			return false;
			
		}
		
		File file = new File(fileName);
			
		ObjectOutputStream output = null;
		
		try {
			
			System.out.println(list);
			output = new ObjectOutputStream (new FileOutputStream(fileName));
			
			output.writeObject(list);
			System.out.println(list);
			output.flush();
			
		} catch ( IOException ioException ) {
			
			return false;
			
		} finally {
			
			try {
				
				output.close();
				
			} catch (IOException ioException) {
				
				return false; 
				
			}
			
		}
		
		return true;
	}
	
	public TreeMap parseData() {
		
		TreeMap<String, Appointment> list = new TreeMap<String, Appointment>();

		FileDialog open = new FileDialog(new Shell(), SWT.OPEN);
		String fileName = open.open();

		if (fileName == null) {
			
			return list;
			
		} 
		
		File file = new File(fileName);
		
		ObjectInputStream input = null;

		try {
				
			input = new ObjectInputStream(new FileInputStream(fileName));
			list = (TreeMap<String, Appointment>) input.readObject( );
			System.out.println(list);
			input.close();
			return list;
				
				
				
		} catch (FileNotFoundException e) {
				
			System.out.println("IO Exception");
				
		} catch (IOException e) {
				
			System.out.println(e);
				
		} catch (ClassNotFoundException e) {

			System.out.println("CNF Exception");
			
		} 
		
		return list;

	}	
		
}
