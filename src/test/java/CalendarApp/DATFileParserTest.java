package CalendarApp;

import static org.junit.Assert.*;

import org.junit.Test;

public class DATFileParserTest {

	@Test
	public void testDATFileParser() {
		
		DATFileParser dat = new DATFileParser();
		dat.setFileName("untitled");
		assertEquals("untitled", dat.getFileName());
		assertEquals("untitled", dat.makeFile().getName());
		assertEquals("untitled", dat.getFileName());
		assertEquals("*.DAT", dat.getFileType());
		
	}

}
