package CalendarApp;

import static org.junit.Assert.*;

import org.junit.Test;

public class AppointmentTest {

	private static final String NAME = "Jack Sparrow";
	private static final int DAY = 29;
	private static final int MONTH = 11;
	private static final int YEAR = 2014;
	private static final int HOUR = 0;
	private static final int MINUTE = 9;
	private static final String PHONE = "555-0123";
	private static final String ADDRESS = "221B Baker Street";
	private static final String DESCRIPTION = "Doctor appointment.";
	private static final String TIME = "00:09";
	private static final String DATE = "20141129";
	private static Appointment TA1 = new Appointment();
	private static Appointment TA2 = new Appointment(NAME, DAY, MONTH, YEAR, HOUR, MINUTE, ADDRESS, PHONE, DESCRIPTION);
	
	@Test
	public void testAppointmentParameters() {
		
		assertEquals(NAME, TA2.getName());
		assertEquals(DAY, TA2.getDay());
		assertEquals(MONTH, TA2.getMonth());
		assertEquals(YEAR, TA2.getYear());
		assertEquals(HOUR, TA2.getHour());
		assertEquals(MINUTE, TA2.getMinute());
		assertEquals(PHONE, TA2.getPhone());
		assertEquals(ADDRESS, TA2.getAddress());
		assertEquals(DESCRIPTION, TA2.getDescription());
		assertEquals(TIME, TA2.getTime());
		assertEquals(DATE, TA2.getDate());
		
	}
	
	@Test
	public void testAppointmentSetters() {
		
		TA1.setName(NAME);
		TA1.setDay(DAY);
		TA1.setMonth(MONTH);
		TA1.setYear(YEAR);
		TA1.setHour(HOUR);
		TA1.setMinute(MINUTE);
		TA1.setPhone(PHONE);
		TA1.setAddress(ADDRESS);
		TA1.setDescription(DESCRIPTION);
		TA1.setTime(TIME);
		TA1.setDate(DATE);
		
		assertEquals(NAME, TA1.getName());
		assertEquals(DAY, TA1.getDay());
		assertEquals(MONTH, TA1.getMonth());
		assertEquals(YEAR, TA1.getYear());
		assertEquals(HOUR, TA1.getHour());
		assertEquals(MINUTE, TA1.getMinute());
		assertEquals(PHONE, TA1.getPhone());
		assertEquals(ADDRESS, TA1.getAddress());
		assertEquals(DESCRIPTION, TA1.getDescription());
		assertEquals(TIME, TA1.getTime());
		assertEquals(DATE, TA1.getDate());
		
	}
	
	@Test
	public void testIsDate() {

		Appointment newTA = new Appointment();
		
		assertEquals(false, TA2.isDate(11, 29, 2014));
		assertEquals(true, newTA.isDate(0, 0, 0));
		
	}
	
	public void testCompareTo() {
		
		Appointment app = new Appointment("I", 29, 11, 2014, 0, 9, "Realize", "My", "Consctructor ");
		Appointment test = new Appointment("Is", 2, 3, 4, 9, 1240, "Way", "Too", "Long");
		assertEquals(0, app.compareTo(TA2));
		assertEquals(-1, TA1.compareTo(TA2));
		assertEquals(1, TA2.compareTo(test));
		
	}
	
}
