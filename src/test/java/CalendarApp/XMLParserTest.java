package CalendarApp;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class XMLParserTest {

	@Test
	public void testXMLParser() {

		XMLParser xml = new XMLParser();
		assertEquals(null, xml.getFile());
		xml.setFile("untitled");
		assertEquals("untitled", xml.getFile().getName());
		assertEquals("*.xml", xml.getFileType());
		xml.setFileName("titled");
		assertEquals("titled", xml.getFileName());

	}

}
