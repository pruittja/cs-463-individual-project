package CalendarApp;

import static org.junit.Assert.*;

import org.junit.Test;

public class DayPlannerTest {

	@Test
	public void testLoad() {
		
		DayPlanner planner = new DayPlanner();
		assertEquals(0, planner.getSize());
		
	}
	
	@Test
	public void testAddRemove() {

		final String KEY = "22001219 12:43";

		DayPlanner planner = new DayPlanner();
		int numAppointments = planner.getSize();
		planner.add(KEY, new Appointment());
		assertEquals(numAppointments + 1, planner.getSize());
		planner.remove(KEY);
		assertEquals(numAppointments, planner.getSize());
		
	}

}
