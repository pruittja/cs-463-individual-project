package CalendarApp;

import static org.junit.Assert.*;

import org.junit.Test;

public class TxtParserTest {

	@Test
	public void testTxtParser() {
		
		TxtParser txt = new TxtParser();
		assertEquals(null, txt.getFile());
		txt.setFile("untitled");
		assertEquals("untitled", txt.getFile().getName());
		assertEquals("*.txt", txt.getFileType());
		txt.setFileName("titled");
		assertEquals("titled", txt.getFileName());

	}

}
