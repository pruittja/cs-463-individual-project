package CalendarApp;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;

public class AddWindow extends Dialog {
	private Text nameText;
	private Text phoneText;
	private Text addressText;
	private DateTime timeTime;
	private DateTime dateTime;
	public Appointment newestApp;
	private Text descriptText;


	/**
	 * Create the dialog.
	 * @param parentShell
	 * @wbp.parser.constructor
	 */
	public AddWindow(Shell parentShell) {
		
		super(parentShell);
		
	}
	
	public String getName() {
		
		return nameText.getText();
		
	}
	
	public String getPhone() {
		
		return phoneText.getText();
		
	}
	
	public String getAddress() {
		
		return addressText.getText();
		
	}
	
	public int getMinute() {
		
		return timeTime.getMinutes();
		
	}
	
	public int getHour() {
		
		return timeTime.getHours();
		
	}
	
	public int getDay() {
		
		return dateTime.getDay();
		
	}
	
	public int getMonth() {
		
		return dateTime.getMonth();
		
	}
	
	public int getYear() {
		
		return dateTime.getYear();
		
	}

	
	public Appointment getNewestApp() {
		
		return newestApp;
		
	}


	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(null);
		
		boolean newAppointment = newestApp != null;
		
		nameText = new Text(container, SWT.BORDER);
		nameText.setBounds(119, 48, 226, 21);
		if (newAppointment) {
			
			nameText.setText(newestApp.getName());
			
		}
		
		phoneText = new Text(container, SWT.BORDER);
		phoneText.setBounds(119, 90, 226, 21);
		if (newAppointment) {
			
			phoneText.setText(newestApp.getPhone());
			
		}
		
		addressText = new Text(container, SWT.BORDER);
		addressText.setBounds(119, 129, 226, 21);
		if (newAppointment) {
			
			addressText.setText(newestApp.getAddress());
			
		}
		
		Label infoLabel = new Label(container, SWT.NONE);
		infoLabel.setBounds(52, 23, 208, 31);
		infoLabel.setText("Please input the relevant information:");
		
		Label phoneLabel = new Label(container, SWT.NONE);
		phoneLabel.setBounds(55, 90, 42, 15);
		phoneLabel.setText("Phone:");
		
		Label addressLabel = new Label(container, SWT.NONE);
		addressLabel.setBounds(47, 132, 50, 15);
		addressLabel.setText("Address:");
		
		Label dateLabel = new Label(container, SWT.NONE);
		dateLabel.setBounds(219, 173, 35, 15);
		dateLabel.setText("Date:");
		
		Label nameLabel = new Label(container, SWT.NONE);
		nameLabel.setBounds(62, 51, 35, 15);
		nameLabel.setText("Name:");
		
		dateTime = new DateTime(container, SWT.BORDER);
		dateTime.setBounds(265, 173, 80, 24);
		if (newAppointment) {
			
			dateTime.setYear(newestApp.getYear());
			dateTime.setMonth(newestApp.getMonth());
			dateTime.setDay(newestApp.getDay());
			
		}
		
		timeTime = new DateTime(container, SWT.BORDER | SWT.TIME | SWT.SHORT);
		timeTime.setBounds(119, 173, 80, 24);
		if (newAppointment) {
			
			timeTime.setMinutes(newestApp.getMinute());
			timeTime.setHours(newestApp.getHour());
			
		}
		
		Label timeLabel = new Label(container, SWT.NONE);
		timeLabel.setBounds(62, 173, 35, 15);
		timeLabel.setText("Time:");
		
		Label descriptLabel = new Label(container, SWT.NONE);
		descriptLabel.setBounds(34, 217, 63, 15);
		descriptLabel.setText("Description:");
		
		descriptText = new Text(container, SWT.BORDER);
		descriptText.setBounds(119, 217, 226, 80);

		return container;
	}

	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		Button button = createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				
				newestApp = new Appointment(nameText.getText(), dateTime.getDay(), 
											dateTime.getMonth(), dateTime.getYear(), 
											timeTime.getHours(), timeTime.getMinutes(), 
											addressText.getText(), phoneText.getText(), descriptText.getText());
		
			}
		});
		
		createButton(parent, IDialogConstants.CANCEL_ID,
				IDialogConstants.CANCEL_LABEL, false);
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(403, 386);
	}
}
