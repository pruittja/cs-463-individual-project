package CalendarApp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLParser implements DataParser {
	
	public boolean saveData(TreeMap<String, Appointment> list) {
		
		FileDialog chooser = new FileDialog(new Shell(), SWT.SAVE);
		
		String fileName = chooser.open();
		
		DocumentBuilderFactory dF = DocumentBuilderFactory.newInstance();
		DocumentBuilder dB;
		try {
			
			dB = dF.newDocumentBuilder();
			
		} catch (ParserConfigurationException e) {
			
			System.out.println("5");
			e.printStackTrace();
			return false;
		}
		
		Document document = dB.newDocument();
		Element rootElement = document.createElement("Planner");
		document.appendChild(rootElement);

		SortedMap<String, Appointment> subMap = list.subMap(list.firstKey(), true, list.lastKey(), true);
		
		int i = 0;
		for (Map.Entry<String, Appointment> entry: subMap.entrySet()) {
			
			
			Element treeMap = document.createElement("TreeMap");
			treeMap.appendChild(document.createElement("Appointment"));
			rootElement.appendChild(treeMap);
			
			Appointment app = entry.getValue();
			String address = app.getAddress();
			String  phone = app.getPhone();
			String name = app.getName();
			int day = app.getDay();
			int  month = app.getMonth();
			int year = app.getYear();
			int hour = app.getHour();
			int minute = app.getMinute();
			String time = app.getTime();
			String date = app.getDate();
			String description = app.getDescription();
			
			System.out.println(i);
			Element addName = document.createElement("name");
			addName.appendChild(document.createTextNode(name));
			treeMap.appendChild(addName);
			
			Element addAddress = document.createElement("address");
			addAddress.appendChild(document.createTextNode(address));
			treeMap.appendChild(addAddress);
			
			
			i++;
		}

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			System.out.println("1");
			e.printStackTrace();
		}
		DOMSource domSource = new DOMSource(document);
		StreamResult streamResult = new StreamResult(new File(fileName + ".xml"));
		try {
			transformer.transform(domSource, streamResult);
		} catch (TransformerException e) {
			System.out.println("2");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("File saved to specified path!");
		return true;
		
		
		
	}
	
	public TreeMap<String, Appointment> parseData() {
		
		return new TreeMap<String, Appointment>();
	}

}
