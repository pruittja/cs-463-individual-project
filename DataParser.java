package CalendarApp;

import java.util.TreeMap;

public interface DataParser {
	
	public boolean saveData(TreeMap<String, Appointment> list);
	
	public TreeMap<String, Appointment> parseData();

}
