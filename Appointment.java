package CalendarApp;

import java.io.Serializable;

public class Appointment implements Serializable, Comparable<Appointment> {

	private String address;
	private String phone;
	private String name;
	private int day;
	private int month;
	private int year;
	private int hour;
	private int minute;
	private String time;
	private String date;
	private String description;
	
	public Appointment() {
		
		address = "";
		phone = "";
		name = "";
		day = 0;
		month = 0;
		year = 0;
		hour = 0;
		minute = 0;
		time = hour + ":" + minute;
		date = month + "/" + day + "/" + year;
		description = "";
		
	}
	
	public Appointment(String name, int day, int month, int year, 
						int hour, int minute, String address, String phone, String description) {
		
		this.address = address;
		this.phone = phone;
		this.name = name;
		this.day = day;
		this.month = month;
		this.year = year;
		this.minute = minute;
		this.hour = hour;
		if (hour <= 9) {
			
			time = "0" + hour + ":" + minute;
			
		} else {
			
			if (minute <= 9) {
				
				time = hour + ":" + "0" + minute;
				
			} else {
				
				time = hour + ":" + minute;
			}
		}
		
		date = String.valueOf(year);
		if (month <= 9) {
			
			date += "0" + month;
			
		} else {
			
			date += month;
			
		}
		
		if (day <= 9) {
			
			date += "0" + day;
			
		} else {
			
			date += day;
			
		}
		
		this.description = description;
		
	}
	
	public boolean isDate(int day, int month, int year) {
		
		return (this.day == day && this.month == month && this.year == year);
		
	}
	
	public int compareTo(Appointment app) {
		
		if (this.hour - app.hour > 1) {
			
			return 1;
			
		} else if (this.hour - app.hour < 1) {
			
			return -1;
			
		} else if (this.minute - app.minute > 1) {
			
			return 1;
			
		} else if (this.minute - app.minute < 1) {
			
			return -1;
			
		} else {
			
			return 0;
			
		}
		
	}
	
	public void setAddress(String address) {
		
		this.address = address;
		
	}
	
	public void setPhone(String phone) {
		
		this.phone = phone;
		
	}
	
	public void setName(String name) {
		
		this.name = name;
		
	}
	
	public void setDay(int day) {
		
		this.day = day;
		
	}
	
	public void setMonth(int month) {
		
		this.month = month;
		
	}
	
	public void setYear(int year) {
		
		this.year = year; 
		
	}
	
	public void setHour(int hour) {
		
		this.hour = hour;
		
	}
	
	public void setMinute(int minute) {
		
		this.minute = minute;
		
	}
	
	public void setTime(String time) {
		
		this.time = time;
		
	}
	
	public void setDate(String date) {
		
		this.date = date;
		
	}
	
	public void setDescription(String description) {
		
		this.description = description;
		
	}
	
	public String getAddress() {
		
		return address;
		
	}
	
	public String getPhone() {
		
		return phone;
		
	}
	
	public String getName() {
		
		return name;
		
	}
	
	public int getDay() {
		
		return day;
		
	}
	
	public int getMonth() {
		
		return month;
		
	}
	
	public int getYear() {
		
		return year;
		
	}
	
	public int getHour() {
		
		return hour;
		
	}
	
	public String getTime() {
		
		return time; 
		
	}
	
	public int getMinute() {
		
		return minute;
		
	}
	
	public String getDate() {
		
		return date;
		
	}
	
	public String getDescription() {
		
		return description;
		
	}
	
	public String toString() {
		
		return "Name: " + name + "\n" +
			   "Date: " + month + "/" + day + "/" + year + "\n" +
			   "Time: " + hour + ":" + minute + "\n" +
			   "Phone: " + phone + "\n" +
			   "Address: " + address + "\n" + " " + "\n";
			   
	}
	
}
