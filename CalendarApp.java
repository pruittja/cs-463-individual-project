package CalendarApp;

import java.util.Calendar;
import java.util.Map;
import java.util.SortedMap;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.StatusLineManager;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class CalendarApp extends ApplicationWindow {

	private Action exitAction;
	private static DayPlanner planner;
	private Button addButton;
	private Button removeButton;
	private Button changeButton;
	private DateTime dateTime;
	private Action saveAction;
	private Action loadAction;
	private static DataParser fileParser;
	private boolean somethingChanged;
	private Table table;
	private ScrolledComposite scrolledComposite;
	private TableColumn tableDate;
	private Action helpAction;
	private Composite container;
	private Action newAction;
	private AddWindow addWindow;
	private DateInputDialog inputD;
	
	/**
	 * Create the application window.
	 */
	public CalendarApp() {
		
		super(null);
		createActions();
		createActions();
		addToolBar(SWT.FLAT | SWT.WRAP);
		addMenuBar();
		addStatusLine();
		planner = new DayPlanner();
		
	}
	
	private void createActions() {
		{
			exitAction = new Action("Exit") {
				
				public void run() {
					
					if (somethingChanged) {
						
						promptSave();
						System.exit(0);
						
					}
					
					
				}
			};
			
			exitAction.setAccelerator(SWT.ALT | SWT.F4);
		}
		{
			saveAction = new Action("Save") {
				
				public void run() {
					
					fileParser.saveData(planner.getAppointmentList());
					
				}
			};
			
			saveAction.setAccelerator(SWT.CTRL | 'S');
		}
		{
			
			loadAction = new Action("Open") {
				
				public void run() {

					if (somethingChanged) {
						
						MessageBox box = new MessageBox(new Shell(), SWT.YES | SWT.NO);
						box.setText("Save?");
						box.setMessage("Would you like to SAVE the CURRENT planner?");
						int input = box.open();
						if (input == SWT.YES) {
							
							fileParser.saveData(planner.getAppointmentList());
							
						}
					}
					
					planner.setAppointmentList(fileParser.parseData());
					
					Calendar cal = Calendar.getInstance();
					dateTime.setDate(cal.DATE, cal.MONTH, cal.YEAR);
					updateText();
					
				}
			};
			loadAction.setAccelerator(SWT.CTRL | 'O');
		}
		{
			helpAction = new Action("Help") {
				
				public void run() {
					
					MessageBox helpDialog = new MessageBox(new Shell(), SWT.OK);
					helpDialog.setText("Help!");
					helpDialog.setMessage("                     F10 - Open the help menu." + "\r\n\r\n" +
										  "           ALT + F4 - Close the application" + "\r\n\r\n" +
										  "           CTRL + S - Save the current planner" + "\r\n\r\n" +
										  "          CTRL + O - Open an existing planner" + "\r\n\r\n" +
										  "          CTRL + N - Create a new planner" + "\r\n\r\n" +
										  "       Add Button - Add an appointment" + "\r\n\r\n" +
										  "Remove Button - Select the TIME of an appointment. Then, press" + "\r\n" + 
										  "                                the remove button to remove an appointment." + "\r\n\r\n\r\n" +
										  " Change Button - Select the TIME of an appointment. Then, press the change" + "\r\n" +
										  "                               button to change one or more attributes of the appointment." + "\r\n\r\n" + 
										  "           Calendar - Using the arrows, choose the month you would like to view." + "\r\n" +
										  "                               Then, select the specific date to view appointments for that day." + "\r\n\r\n" +
										  "Also, clicking the date at the top of the LIST will allow manual input of a date" + "\r\n\r\n" +
										  "Click on the MONTH name in the calendar view expand to quickly select the month.");

					helpDialog.open();
					
				}
			};
			helpAction.setAccelerator(SWT.F10);
		}
		{
			newAction = new Action("New Planner") {
				
				public void run() {
					
					if (somethingChanged) {
						
						MessageBox box = new MessageBox(new Shell(), SWT.YES | SWT.NO);
						box.setText("Save?");
						box.setMessage("Would you like to save the current planner?");
						int input = box.open();
						if (input == SWT.YES) {
							
							fileParser.saveData(planner.getAppointmentList());
							
						}
					}
					
					planner.getAppointmentList().clear();
					updateText();
					
				}
			};
			newAction.setAccelerator(SWT.CTRL | 'N');
		}
	}

	/**
	 * Create contents of the application window.
	 * @param parent
	 */
	@Override
	protected Control createContents(Composite parent) {
		
		container = new Composite(parent, SWT.NONE);
		container.setLayout(null);
		
		addButton = new Button(container, SWT.NONE);
		
		addButton.setBounds(718, 0, 129, 25);
		addButton.setText("Add Appointment");
		
		addButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent arg0) {
				
				addAppointment();
				
			}
			
		});
		
		removeButton = new Button(container, SWT.NONE);
		removeButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				
				removeAppointment();
				
			}
			
		});
		
		removeButton.setBounds(718, 56, 129, 25);
		removeButton.setText("Remove Appointment");
		
		changeButton = new Button(container, SWT.NONE);
		changeButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseUp(MouseEvent e) {
				
				changeAppointment();
				
			}
		});
		
		changeButton.setBounds(718, 111, 129, 25);
		changeButton.setText("Change Appointment");
		
		dateTime = new DateTime(container, SWT.CALENDAR);
		
		dateTime.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseDown(MouseEvent arg0) {
				
				updateText();
				
			}
			
		});
		
		dateTime.setBounds(0, 0, 226, 145);
		
		Label picLabel1 = new Label(container, SWT.NONE);
		picLabel1.setImage(SWTResourceManager.getImage("U:\\CS 463\\Individual Project\\DataManagement\\src\\main\\java\\CalendarApp\\Puppy_Image.jpg"));
		picLabel1.setBounds(223, 0, 721, 145);
		
		Label picLabel2 = new Label(container, SWT.NONE);
		picLabel2.setImage(SWTResourceManager.getImage("U:\\CS 463\\Individual Project\\DataManagement\\src\\main\\PuppyImage2.jpg"));
		picLabel2.setBounds(0, 142, 226, 465);
		
		Label picLabel3 = new Label(container, SWT.NONE);
		picLabel3.setImage(SWTResourceManager.getImage("U:\\CS 463\\Individual Project\\DataManagement\\src\\main\\java\\PuppyImage3.jpg"));
		picLabel3.setBounds(652, 142, 292, 465);
		
		scrolledComposite = new ScrolledComposite(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		scrolledComposite.setBounds(223, 142, 432, 465);
		scrolledComposite.setExpandHorizontal(true);
		scrolledComposite.setExpandVertical(true);
		
		table = new Table(scrolledComposite, SWT.MULTI);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				
				String selection = (String) (table.getSelection())[0].getData();
				
			}
		});
		table.setHeaderVisible(true);
		table.setLinesVisible(false);
		
		tableDate = new TableColumn(table, SWT.CENTER);
		tableDate.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				manualDate();
				
			}
		});
		tableDate.setResizable(false);
		tableDate.setWidth(406);

		scrolledComposite.setContent(table);
		scrolledComposite.setMinSize(table.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		
		addWindow = new AddWindow(this.getShell());
		
		updateText();

		return container;
	}
	
	private void promptSave() {
			
		MessageBox box = new MessageBox(new Shell(), SWT.YES | SWT.NO);
		box.setText("Save?");
		box.setMessage("Would you like to save the current planner?");
		int input = box.open();
		if (input == SWT.YES) {
				
			fileParser.saveData(planner.getAppointmentList());
				
		}
		
	}
	
	private void updateText() {
		
		table.removeAll();		
		tableDate.setText(((dateTime.getMonth() + 1) + "/" + dateTime.getDay() + "/" + dateTime.getYear()));
		
		String startKey = makeKey("00:00");
		String endKey = makeKey("23:59");
		
		SortedMap<String, Appointment> subMap = planner.getAppointmentList().subMap(startKey, endKey);
				
		for (Map.Entry<String, Appointment> entry: subMap.entrySet()) {
			if (entry.getValue() != null) {

				if (entry.getValue().isDate(dateTime.getDay(), dateTime.getMonth(), dateTime.getYear())) {

					TableItem time = new TableItem(table, SWT.NONE);
					time.setText("Time: " + entry.getValue().getTime());
					TableItem name = new TableItem(table, SWT.NONE);
					name.setText("Name: " + entry.getValue().getName());
					TableItem phone = new TableItem(table, SWT.NONE);
					phone.setText("Phone: " + entry.getValue().getPhone());
					TableItem address = new TableItem(table, SWT.NONE);
					address.setText("Address: " + entry.getValue().getAddress());
					TableItem description = new TableItem(table, SWT.NONE);
					description.setText("Description: " + entry.getValue().getDescription());
					TableItem blankItem = new TableItem(table, SWT.NONE);
					blankItem.setText("");
				
				}
			
			}
			
		}
		
	}
	
	public void removeAppointment() {
		
		String selected = table.getSelection()[0].getText();
		if (selected != null && selected.substring(0, 4).equals("Time")) {
			
			String key = makeKey(selected.substring(6));
			MessageBox box = new MessageBox(new Shell(), SWT.YES | SWT.NO);
			box.setText("Are you sure?");
			box.setMessage("Are you sure you want to remove this appointment?");
			int input = box.open();
			if (input == SWT.YES) {
				
				planner.remove(key);
				updateText();
				
			}
			
		} else {
			
			MessageBox message = new MessageBox(new Shell(), SWT.OK);
			message.setText("Error");
			message.setMessage("You must select the TIME of an appointment " + "\r\n" + 
							   " to remove any appointments.");
			message.open();
			
		}
		
	}
	
	public void changeAppointment() {
		
		String selected = table.getSelection()[0].getText();
		if (selected != null && selected.substring(0, 4).equals("Time")) {
			
			String key = makeKey(selected.substring(6));
			Appointment app = planner.getAppointmentList().get(key);
			AddWindow prompt = new AddWindow(this.getShell());
			prompt.newestApp = app;
			prompt.open();
			
			Appointment newestApp = prompt.getNewestApp();
			planner.remove(key);
			planner.add((newestApp.getDate() + " " + newestApp.getTime()), newestApp);

			updateText(); 
			somethingChanged = true;
			
		} else {

				MessageBox message = new MessageBox(new Shell(), SWT.OK);
				message.setText("Error");
				message.setMessage("You must select the TIME of an appointment " + "\r\n" + 
								   " to change any attributes of an appointment.");
				message.open();
			
		}
		
	}
	
	public String makeKey(String time) {
		
		int day = dateTime.getDay();
		int month = dateTime.getMonth();
		int year = dateTime.getYear();
		
		String key = String.valueOf(year);
		if (month <= 9) {
			
			key += "0" + month;
			
		} else {
			
			key += month;
			
		}
		
		if (day <= 9) {
			
			key += "0" + day;
			
		} else {
			
			key += day;
			
		} 
		
		key += " " + time;
		
		 return key;
	}
	
	public void addAppointment() {
		
		AddWindow prompt = new AddWindow(null);
		prompt.open();
		
		Appointment newestApp = prompt.getNewestApp();
		if (planner.getAppointmentList().containsKey((newestApp.getDate() + " " + newestApp.getTime()))) {
			
			MessageBox box = new MessageBox(new Shell());
			box.setText("Illegal Forward Motion");
			box.setMessage("You already have an appointment scheduled for: " + "\r\n" +
						   newestApp.getHour() + ":" + newestApp.getMinute() + " on " + 
						   newestApp.getMonth() + "/" + newestApp.getDay() + "/" + newestApp.getYear());
			box.open();
			return;
			
		}
		
		planner.add((newestApp.getDate() + " " + newestApp.getTime()), newestApp);
			
		updateText(); 
		somethingChanged = true;
			
	}
	
	public void manualDate() {
		
		inputD = new DateInputDialog(new Shell());
		inputD.open();
		String date = inputD.getInputText();
		
		if (date != null) {
			
			StringTokenizer sT = new StringTokenizer(date, "/", false);
			dateTime.setMonth(Integer.parseInt(sT.nextToken() + 1));
			dateTime.setDay(Integer.parseInt(sT.nextToken()));
			dateTime.setYear(Integer.parseInt(sT.nextToken()));
			updateText();
			
		}
		
		updateText();
		
	}


	/**
	 * Create the menu manager.
	 * @return the menu manager
	 */
	@Override
	protected MenuManager createMenuManager() {
		MenuManager menuManager = new MenuManager("menu");
		
		MenuManager file = new MenuManager("File");
		menuManager.add(file);
		file.add(newAction);
		file.add(saveAction);
		file.add(loadAction);
		file.add(new Separator());
		file.add(exitAction);
		
		MenuManager helpMenu = new MenuManager("Help");
		menuManager.add(helpMenu);
		helpMenu.add(helpAction);
		menuManager.setVisible(true);
		return menuManager;
	}

	/**
	 * Create the toolbar manager.
	 * @return the toolbar manager
	 */
	@Override
	protected ToolBarManager createToolBarManager(int style) {
		ToolBarManager toolBarManager = new ToolBarManager(style);
		return toolBarManager;
	}

	/**
	 * Create the status line manager.
	 * @return the status line manager
	 */
	@Override
	protected StatusLineManager createStatusLineManager() {
		StatusLineManager statusLineManager = new StatusLineManager();
		return statusLineManager;
	}
	
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String args[]) {
		
		ApplicationContext context = new FileSystemXmlApplicationContext("U:\\CS 463\\Individual Project\\DataManagement\\src\\main\\java\\CalendarApp\\Beans.xml");
		fileParser = (DataParser)context.getBean("dataParser");
		planner = new DayPlanner();
		
		try {
			CalendarApp window = new CalendarApp();
			window.setBlockOnOpen(true);
			window.open();
			Display.getCurrent().dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Configure the shell.
	 * @param newShell
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Day Planner");
	}

	/**
	 * Return the initial size of the window.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(663, 628);
	}
}


